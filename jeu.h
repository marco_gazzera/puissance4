#include <stdbool.h>



/* Puissance 4 
Ecriture du programme en C.
Regle du Jeu:
A- Placer un jeton de sa couleur dans une colonne à tour de role
B- Celui qui aligne 4 pions de sa couleur a gagné
C- Si toutes les cases sont remplis et qu'aucun joueur n'a réussi, la partie est nulle

Fichiers: jeu.c, jeu.h, main.c

Fonctionnalités:
0- Interface graphique sommaire d'un plateau 7 lignes et 7 colonnes
1- Placer un jeton dans une colonne
2- Tourner un plateau de 90 degrés à droite ou à gauche (les lignes deviennent colonnes)
3- Inverser la vue (retourner le plateau)

Fonctionnalités avancées:
4- Joueur autonome
5- Sauvegarde d'une partie en cours
6- Lecture d'un fichier d'une partie sauvegardée
*/



/*-------------------------------définitions et déclarations----------------------------------------*/
#define  VIDE ' '
#define ROUGE 'x'
#define JAUNE 'o'

#define NB_COLONNES 7
#define NB_LIGNES 7

#define TRUE 1
#define FALSE 0

#define RENVERSE 2
#define DROITE 1
#define GAUCHE 0

#define MAX_NB_PARTIES 100


#define MAX_NOM_FICHIER 50
#define NOM_FICHIER "sauvegarde"

const int MAX_FICHIER = MAX_NB_PARTIES*(NB_LIGNES*NB_COLONNES+10);
// nouveau type Joueur 
typedef struct joueur Joueur ;
//  nouveau type Plateau
typedef struct plateau Plateau ;
/* __________________________Initialisations des joueurs____________________________ */

void definition_joueur(int joueur, int human);
void definition_joueurs(char choix_mode);
/*-----------------------------------------------------------------------------------*/

/*-------------------------------fonctions outils------------------------------------*/
void sleep(unsigned long int n);
int choix_rand(int N);
void initMatrice2D(char matrice[NB_LIGNES][NB_COLONNES]);
void initMatriceParties();
void setMatParties();
bool compare(int nb1, int nb2, int nb_alea);
void reprendre_partie();


/*-------------------------------interface graphique---------------------------------*/


/*------------------------------------menus------------------------------------------*/
char choix_mode_de_jeu();
void demande_coup(char *choix);
void menu_option_de_jeu();
/*-----------------------------------------------------------------------------------*/
/*0- Interface graphique sommaire d'un plateau 7 lignes et 7 colonnes,
 affiche la matrice de jeu.
   @args: void
   @return: void */
void affichage_plateau();
/*-----------------------------------------------------------------------------------*/





/*------------------------------fonctions de contrôle   -----------------------------*/
bool coupvalide(int colonne);
void do_coup(char choix);
bool check();
char* recupereCoup(int coup);
/*-----------------------------------------------------------------------------------*/





/*------------------------------fonctionnalités de base------------------------------*/
/*1- Placer un jeton dans une colonne de la matrice de jeu
   @do:   modifie la matrice de jeu grille.matrice : variable globale
   @args: colonne numero de la colonne
   @uses: coupvalide(colonne) 
   @return: inteen possible ou pas si la colonne est pleine ou pas */

bool placer_jeton(int colonne);


/* 2- Tourner un plateau de 90 degrés à droite ou à gauche (les lignes deviennent colonnes)
    @do:     modifie la matrice de jeu grille.matrice : variable globale
   @args:   sens
   @uses:   initMatrice2D() , tmpMat : variable globale
   @return: void */
 void tourner_plateau(int sens);


/* 3- Tourner un plateau de 180 degrés (le haut se retrouve en bas)
   @do:     modifie la matrice de jeu grille.matrice : variable globale
   @args:   void 
   @uses:   initMatrice2D() , tmpMat : variable globale
   @return: void */
 void inverser();
/*----------------------------------------------------------------------------------*/


 /*------------------------------fonctionnalités avancées-----------------------------*/

/* 4- Joueur autonome joue avec intelligence artificielle de niveau 1
   @do:  simule l'urgence: contrer ou gagner le plus vite possible
		 1. compare 4 matrices: la normale, celle inversée, celle tournée à droite et celle 
		 tournée à gauche.
		 2. utilise la fonction check pour savoir ou placer son jeton dans l'urgence
		 		verifie si le joueur en face est peut aligner 4 pions au coup suivant 
		 		 en utilisant J,I, D ou G
		 			si oui joue pour le contrer (dans l'ordre J->I->G->D)
		 			sinon charche à aligner 4 jetons
   @uses:	initMatrice2D() , tmpMat : variable globale
   @args: 
   @return: void */
 void JoueurIA();


/* 5- Sauvegarde d'une partie en cours
   @do:     copie tous les coups de la partie dans un fichier
   @args:   void 
   @uses:   matrice dynamique qui contient la partie à sauvegarder éventuellement 
   @return: void */
 void sauvegarde();

 /* 6- Lecture d'une partie sauvegardée
   @do:     lit un fichier et reproduit toutes les grilles avec leurs numéro de coup
			demande à l'utilisateur s'il veut reprendre la partie à la suite ou à un coup choisit

   @args:   void 
   @uses:   fichier externe
   @return: void */
 bool lecture();
