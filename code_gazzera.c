#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdbool.h>
 
#define GAP(a,b)    (((a)>(b))?((a)-(b)):((b)-(a)))
#define N 3

/* --------------------------------------*/
//prototypes
int initialisation (int *nbtires);
int calcul(int cible, int *tab, int nbch);
int operation(int n1, int n2, int operateur);
void sleep(unsigned long int n);
void sabilier();

 
/* --------------------------------------*/
int best_tot = 0;
int best_gap = 999;
char str_result[555],str_tmp[555];
 
 
int main(){
    int nbc, nbch=6;    //nb cible
    int nbtir[6];
 
    //appel des fonctions
    nbc=initialisation(nbtir);
 
    if ( ! calcul(nbc,nbtir,nbch) ) {
        calcul(best_tot,nbtir,nbch);
        printf("Solution approchee : \n");
    }
 	
    else{
    //sleep(5000);
    printf("-------------------------------------------------------------------------\n");
	printf("Le compte est bon \n");
 	printf("-------------------------------------------------------------------------\n");
 	
	}
    //affichage solution
    
    return 0;
}

/* --------------------------------------*/
 
//Initialisation du jeu : 6 nbs al�atoires tir�s + 1 nb � trouver
int initialisation (int *nbtires){
    int indice,i,j;
    int nbcible;
    int nbpossible[14]={1,2,3,4,5,6,7,8,9,10,25,50,75,100}; //valeurs d�part possibles
 
    srand(time(0));
    printf("_________________________________________________________________________\n");
    printf("\n");
    printf("--------------------------- LE COMPTE EST BON ? -------------------------\n");
    printf("_________________________________________________________________________\n");
    
    printf("\n");
    
    printf("-------------------------------------------------------------------------\n");
    
    printf("Tirage : ");
	
	 
  for (i=0;i<6;i++)    {
    nouveau:    //etiquette
    indice= rand()%14;
 	
        for(j=0;j<6;j++)        {
            if (nbtires[j]==nbpossible[indice]) goto nouveau; //condition pour ne pas avoir 2 fois le meme nombre
        }
    
    nbtires[i]=nbpossible[indice];   //nombres tir�s au hasard
    printf("%i \t",nbpossible[indice]);
    
    }
    nbcible=(rand()%900)+100;//nombre � trouver enter 100 et 999
    printf("\n-------------------------------------------------------------------------\n");
	//sleep(5000);
	printf("\n-------------------------------------------------------------------------");
    printf("\n\nVous devez trouver : %i \n",nbcible);
    printf("En additionnant, soustrayant, multiplant, divisant les nombres ci dessus \n");
 	printf("\n-------------------------------------------------------------------------\n");
 	sabilier();
 	//sleep(10000000);
 	printf("-------------------------------------------------------------------------\n");
 	
    return nbcible;
}
 
//calcul de la solution du nombre � trouver
int calcul(int cible, int *tab, int taille){
    int i,j,o;  //boucles sur nb1, nb2, operation
    int t[6];
    char operateurs[4]={'+','-','*','/'};   //les 4 op�rations
 
    for (i=0; i<taille-1; i++)    {
        for (j=i+1; j<taille; j++)        {
            for(o=0;o<4;o++)            {
                memcpy(t,tab,sizeof(int)*6);
                t[i]=operation(tab[i],tab[j],o);//t contient les r�sultat des op�rations entre 2 nombres du tirage
                //si on n'a pas encore trouv�, on continue de chercher
                if (!t[i]) continue;
                //si on a trouv�, alors on affiche la solution
                if(t[i] == cible)                {
                    
                    printf("%d %c %d = %d\n", tab[i],operateurs[o],tab[j],t[i]);
                    return 1;
                }
 

 
                //on retrie le tableau
                t[j]=t[taille-1];
 
                if (calcul(cible,t, taille-1)) { //appel r�cursif pr recommencer

                    printf("%d %c %d = %d\n", tab[i],operateurs[o],tab[j],t[i]);
                    return 1;
                }
            }
        }
    }
    return 0;
}
 
 
//fonction qui calcule les op�rations entre 2 nb tir�s
int operation(int n1, int n2, int operateur){
    switch (operateur)    {
        case 0:        {
            return (n1+n2);
            break;
        }
        case 1:        {
            return GAP(n1,n2);
            break;
        }
 
        case 2:        {
            return (n1*n2);
            break;
        }
 
        case 3:        {
            if (0== n1%n2) return (n1/n2);
            break;
        }
        default:        {
            printf("ERROR!!!\n");
            break;
        }
    }
    return 0;
}

void sleep(unsigned long int n) {
        /* boucle vide parcourue (n * 100000) fois*/
        int i = 0;
        unsigned long int max = n * 100000;
        do {
                /* Faire qqch de stupide qui prend du temps */
                i++;
        }
        while(i <= max);
}

 void sabilier(){
 	int i, j, k;
 	
	for(i=N; i>=1; i--){
 		for(j=0; j<N-i; j++){
 			printf(" ");
 		}
        //sleep(1000);
 		for(k=0; k<2*i+1; k++){
 			printf("*");
            //sleep(1000);	
		}
		printf("\n");
	}
	
	for(i=0; i<N; i++){
 		for(j=0; j<N-i; j++){
 			printf(" ");
            //sleep(1000);
 		}
 		for(k=0; k<2*i+1; k++){
 			printf("*");
            //sleep(1000);	
		}
		printf("\n");
	}
	printf("*******");
	printf("\n");
	
}

