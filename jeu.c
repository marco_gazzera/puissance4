#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>

#include "jeu.h"



/* Puissance 4 
Ecriture du programme en C.
Regle du Jeu:
A- Placer un jeton de sa couleur dans une colonne à tour de role
B- Celui qui aligne 4 pions de sa couleur a gagné
C- Si toutes les cases sont remplis et qu'aucun joueur n'a réussi, la partie est nulle

Fichiers: jeu.c, jeu.h, main.c

Fonctionnalités:
0- Interface graphique sommaire d'un plateau 7 lignes et 7 colonnes
1- Placer un jeton dans une colonne
2- Tourner un plateau de 90 degrés à droite ou à gauche (les lignes deviennent colonnes)
3- Inverser la vue (retourner le plateau)

Fonctionnalités avancées:
4- Joueur autonome
5- Sauvegarde d'une partie en cours
6- Lecture d'un fichier d'une partie sauvegardée
*/


/* ___________________________Déclarations et Initialisations_______________________________ */

/* _____________________Déclaration des variables globales du jeu___________________________ */

//joueur 
struct joueur{
    char nom[50]; //Joueur 1 ou Joueur 2
    bool humain; //humain ou machine
    int couleur; // define couleur ROUGE JAUNE
    char jeton;
};
//tableau de joueurs de type Joueur défini dans le header
Joueur Joueurs[2];
//numero du joueur à qui est le tour de jouer
int leJoueur; 


// plateau de jeu
struct plateau{
   char matrice[NB_LIGNES][NB_COLONNES];
   //char** matrice (char**) malloc((char*) sizeof(char)*7);
   // le plateau est modélisé par un tableau à deux dimensions
  // int position; //le plateau est tournée à droite:DROITE, à gauche:GAUCHE, à 180°:RENVERSE
};
//grille de jeu de type Plateau défini dans le header
Plateau grille;
// case
typedef struct{
  int dg;
  int vert;
  int dd;
  int horz;
}Case ;
//matrice de vérification 
Case game[NB_COLONNES][NB_LIGNES];
//matrice temporaire
char tmpMat[NB_LIGNES][NB_COLONNES];
// parties
char parties[MAX_NB_PARTIES*(NB_LIGNES*NB_COLONNES+10)]; 
//pour l'IA niveau 0
int nbCoups;

/* ______________________________Initialisations des joueurs_______________________________ */

void definition_joueur(int joueur, int human){
    char leNom[50];
    if (human) {
        do {
          printf("Quel est ton nom joueur%d ? ", joueur+1);
          int j;
          j = scanf(" %50s",leNom);
          if (j != 1) {
           printf("Ce nom porte a confusion, peux tu en donner un autre ? ;)\n"); 
           while (!feof(stdin) && !ferror(stdin) && getc(stdin) != '\n');
          }
        } while (leNom==Joueurs[!joueur].nom);
        printf("Bienvenue %s ! Tu es le joueur %d, n\'oublie pas.\n",leNom, joueur+1);
    } 
    else {
            char * tmp = (joueur)?"IA UN":"IA DEUX";
            strcpy(leNom,tmp);
            printf("Le joueur %d est une IA nommee %s... Bienvenue a elle! ;)\n",joueur+1, leNom);
            sleep(2500);
            printf(" <------- Merci Ordi -------> \n");
    } 
    strcpy(Joueurs[joueur].nom,leNom);
    Joueurs[joueur].humain = human;
}
/* __________________________________________________________________ */
void definition_joueurs(char choix_mode) {
    bool nature_j1, nature_j2;
    switch(choix_mode){
            case 'A':
                    nature_j1 = FALSE;
                    nature_j2 = FALSE;
                    break;
            case '1':
                    nature_j1 = TRUE;
                    nature_j2 = FALSE;
                    break;
            case '2':
                    nature_j1 = TRUE;
                    nature_j2 = TRUE;
                    
                    break;
     }
     definition_joueur(0, nature_j1);
     definition_joueur(1, nature_j2);            
}
/* __________________________________________________________________ */



/*-------------------------------fonctions outils------------------------------------*/
//simuler artificiellement l'intelligence ;)
void sleep(unsigned long int n) {
        /* boucle vide parcourue (n * 100000) fois*/
        int i = 0;
        unsigned long int max = n * 100000;
        do {
                /* Faire qqch de stupide qui prend du temps */
                i++;
        }
        while(i <= max);
}

// Choix aléatoire nombre entre 0 et N pour intro du jeu pour savoir qui commnece
int choix_rand(int N){

   int tab[N];
   int first = 0;
   int index;
   int rn;
  srand((unsigned)time(NULL));
   if (first == 0)   {
      int i;

     srand((unsigned)time(NULL));
      for (i = 0; i < N; i++)
         tab[i] = (int)(rand() / (double) RAND_MAX * (N-1) + 1);
      first = 1;
   }


   index = (int)(rand() / (double) RAND_MAX * (N - 1));
   rn = tab[index];
   tab[index] = (int)(rand() / (double) RAND_MAX * (N-1) + 1);
   return (rn);
}

bool compare(int nb1, int nb2, int nb_alea){
    //TRUE: joueur1 gagne
  return ( abs(nb1 - nb_alea) < abs(nb2 - nb_alea) ) ? 1 : 0;

}

//initialisation de la matrice de jeu
void initMatrice2D(char matrice[NB_LIGNES][NB_COLONNES])
{
    int w, h;
    for(w = 0; w < NB_LIGNES; w++)
    {
        for(h = 0; h < NB_COLONNES; h++)
        {
            matrice[w][h]=' '; // on initialise la tableau pour qu'il soit vide au debut
        }
    }
}
//initialisation de la matrice parties
void initMatriceParties(char matrice[100][NB_LIGNES*NB_COLONNES+10])
{
    int w, h;
    for(w = 0; w < 100; w++)
    {
        for(h = 0; h < NB_LIGNES*NB_COLONNES+9; h++)
        {
            matrice[w][h]=' '; // on initialise la tableau pour qu'il soit vide au debut
        }
    }
}
//copie d'une matrice 
void copyMatrice2D(char matrice[NB_LIGNES][NB_COLONNES],char matrice_tmp[NB_LIGNES][NB_COLONNES])
{
    int w, h;
    for(w = 0; w < NB_LIGNES; w++)
    {
        for(h = 0; h < NB_COLONNES; h++)
        {
            matrice[w][h]=matrice_tmp[w][h]; // on initialise la tableau pour qu'il soit vide au debut
        }
    }
}
//copie du coup dans la matrice temporaire globale parties
void setMatParties() {
  #define MAX_LIGNE 100
   char ligne[MAX_LIGNE];
   strcpy(ligne, "");

   for (int i=0;i<NB_LIGNES;i++) {
      for (int j=0;j<NB_COLONNES;j++) {
        if (grille.matrice[i][j]==' ') strcat(ligne, "0");
        if (grille.matrice[i][j]=='o') strcat(ligne, "1");
        if (grille.matrice[i][j]=='x') strcat(ligne, "2");  
      }
      strcat(ligne, "*");
    }
    strcat(ligne, "|");
    strcat(parties,ligne);
 }

void au_revoir() {
  printf("Au revoir!\n");
  exit(0);
}

//copie du coup dans la matrice temporaire globale parties
void setCoupMat(char* cp) {
  //strcpy(coup, couptmp);
   char* ligne = strtok(cp, "*");
   int i=0;
   while (ligne != NULL) {
      for (int j=0;j<7;j++) {
        if (ligne[j]=='0') grille.matrice[i][j] = ' ';
        if (ligne[j]=='1') grille.matrice[i][j] = 'o';
        if (ligne[j]=='2') grille.matrice[i][j] = 'x';
      }
      ligne = strtok(NULL, "*");
      i++;
   }
 }

char* recupereCoup(int coup) {

   char* ligne = strtok(parties, "|");
  int i=0;
   while (ligne != NULL) {

      if (i == coup) 
        {
          printf ("a recupere ligne %s",ligne);
          return ligne;
        }
      ligne = strtok(NULL, "|");
      i++;
   }
   return NULL;
}
/*-------------------------------interface graphique---------------------------------*/


/*------------------------------------menus------------------------------------------*/
char choix_mode_de_jeu() {
    char choix_mode;
    
   
      printf("Bienvenue a Puissance 4, fais un choix s'il te plait:  \n");
      
      do {
        printf("  [ (A)utomatique (deux IAs!),(1) joueur (contre une IA!),(2) joueurs,(Q)uitter ] :");
        fflush(stdout);
        int j;
        j = scanf(" %c", &choix_mode);
        if (j != 1) {
         printf("Choisis la bonne option stp ;)\n");
         while (!feof(stdin) && !ferror(stdin) && getc(stdin) != '\n');
        }
      } while ((choix_mode!='1')&&(choix_mode!='2')&&(choix_mode!='A')&&(choix_mode!='Q'));
    
    if (choix_mode=='Q') {
       au_revoir();
    }
    return choix_mode;
}



void demande_coup(char *choix){
  *choix=' ';
  printf("A toi %s", Joueurs[leJoueur].nom);
 

  if (Joueurs[leJoueur].humain==false) {
      printf("  [ (J)ouer (I)nverser (D)roite (G)auche ] :");
      sleep(1000);
      char choix_tab[7] = "JJJJIDG";
      fflush(stdout);
      //choisi un coup par "IA" et le premier doit être obligatoirement 
      //mettre un jeton sinon cela ne fait pas très intelligent surtout si l'IA commence...
      sleep(6000);
      if (nbCoups>2) {
        int alea = choix_rand(7);
        *choix = choix_tab[alea];
      }
      else {
        *choix = 'J';
      }
      printf(" %c \n",*choix);
      sleep(2500);   
  }
  else {
    do {
      printf(" \n [ (J)ouer (I)nverser (D)roite (G)auche laisser la (M)ain (Q)uitter (S)auvegarder (C)harger une partie] :");
      fflush(stdout);
      int j;
      j = scanf(" %c", choix);
      if (j != 1) {
        printf("Choisis la bonne option stp ;)\n");
        while (!feof(stdin) && !ferror(stdin) && getc(stdin) != '\n');
      }
    } while ((*choix!='J')&&(*choix!='I')&&(*choix!='D')&&(*choix!='G')&&(*choix!='M')&&(*choix!='Q')&&(*choix!='S')&&(*choix!='C'));
    //si le joueur fait une sauvegarde il ne perd pas son tour
    
    sleep(2500);
  }
}

/*0- Interface graphique sommaire d'un plateau 7 lignes et 7 colonnes,
 affiche la matrice de jeu.
   @args: void
   @return: void */

void affichage_plateau() 
{
    printf("J'affiche le plateau du coup no %d!\n",nbCoups);
    sleep(5000);
    int w, h;
    printf("|");
    for(w = 0; w < NB_COLONNES; w++)
        printf(" %d ", w+1);  // on affiche le numero de la colonne
    printf("|\n");
 
    printf("|");
    for(w = 0; w < NB_COLONNES; w++)
        printf("___"); // une ligne 'souligné' pour dessiner le cadre
    printf("|\n");
 
    for(h = NB_LIGNES-1 ; h >= 0; h--) // on affiche la ligne du haut en bas et on descend pour avoir l'affichage dans le bon sens
    {
        printf("|");
        for(w = 0; w< NB_COLONNES; w++)
        {
            printf(" %c ", grille.matrice[h][w]);
        }
        printf("|\n");
    }
    printf("|");
    for(w = 0; w < NB_COLONNES; w++)
        printf("___"); // une ligne 'souligné' pour dessiner le cadre
    printf("|\n");
     
}
void affichage_partie_chargee() {

    const char* separateur = "|";
    char* coup ;
    char* ligne;
    nbCoups = 0;
    char* rest;
    printf("La partie est chargée!\n");
    for ((coup = strtok_r(parties,separateur, &rest));coup!=NULL;coup = strtok_r(NULL,separateur, &rest)) {
      nbCoups++;

     
      char* rest_cp;
      int i=0;
      for ((ligne = strtok_r(coup,"*", &rest_cp));ligne!=NULL; ligne = strtok_r(NULL,"*", &rest_cp)) {
        for (int j=0;j<7;j++) {
         if (ligne[j]=='0') grille.matrice[i][j] = ' ';
         if (ligne[j]=='1') grille.matrice[i][j] = 'o';
         if (ligne[j]=='2') grille.matrice[i][j] = 'x';
       }
     
      i++;
     }
     
      affichage_plateau();
      
   }
}
void reprendre_partie(){
  const char* separateur = "|";
  
  int coup;
  do {
        printf("A quel coup veux-tu reprendre la partie? ");
        fflush(stdout);
        int j;
        j = scanf(" %3d", &coup);   
        if (j != 1) {
        printf("Choisis la bonne option stp ;)\n"); 
        while (!feof(stdin) && !ferror(stdin) && getc(stdin) != '\n');
        }
    } while (coup>nbCoups);
    nbCoups=coup; 

    char* rest = recupereCoup(nbCoups);

    int i=0;
    char* ligne = strtok(rest,"*");
    while (ligne != NULL) {
        for (int j=0;j<7;j++) {
         if (ligne[j]=='0') grille.matrice[i][j] = ' ';
         if (ligne[j]=='1') grille.matrice[i][j] = 'o';
         if (ligne[j]=='2') grille.matrice[i][j] = 'x';
      }
      ligne = strtok(rest,"*");
      i++;
     }
      affichage_plateau();


}

/*------------------------------fonctions de contrôle   -----------------------------*/
// do_coup: choix des différentes options du jeu qui sont proposées dans le menu
void do_coup(char choix){
  int colonne;
  char col_char;
  bool tmp = FALSE;
  int nb;
  char matrice[NB_LIGNES][NB_COLONNES];
  switch(choix){

            case 'J':
                      printf(" %s, tu veux ajouter un jeton... Reflechis-bien...\n",Joueurs[leJoueur].nom);
                     sleep(5000);
                     if (Joueurs[leJoueur].humain==false) {
                       printf(" %s reflechit pour choisir la colonne...\n",Joueurs[leJoueur].nom);
                       sleep(5000);
                       colonne = choix_rand(7);

                     }
                     else {
                      
                      do {
                          printf("Dans quelle colonne veux-tu mettre le jeton (nombre de 1 a %d)? ",NB_COLONNES);
                          fflush(stdout);
                          int j;
                          j = scanf(" %c", &col_char);   
                          if (j != 1) {
                          printf("Choisis la bonne option stp ;)\n"); 
                          while (!feof(stdin) && !ferror(stdin) && getc(stdin) != '\n');
                          }
                      } while ((col_char!='1')&&(col_char!='2')&&(col_char!='3')&&(col_char!='4')&&(col_char!='5')&&(col_char!='6')&&(col_char!='7'));
                     colonne = (int)col_char- (int)48;
                     }
                    
                    printf("Donc %s,tu veux mettre ton jeton %c dans la colonne %d ? Je vais verifier si c'est possible...\n",Joueurs[leJoueur].nom, Joueurs[leJoueur].jeton, colonne);
                    sleep(5000);
                    tmp = placer_jeton(colonne);
                     

                    break;


            case 'I':
                    inverser();
                    break;

            case 'D':
                    tourner_plateau(DROITE);

                    break;

            case 'G':
                    tourner_plateau(GAUCHE);
                    break;

             case 'M':
                    definition_joueur(leJoueur, FALSE);
                    break;
 
             case 'C':
                    
                    if (lecture()) {
                      affichage_partie_chargee();
                      int coup;
                     }
                     else {
                      printf("Essaie de charger la partie une prochaine fois...\n");
                     }
                    break;
            
            case 'Q':
                    au_revoir();
                    break;

            case 'S':
                    sauvegarde();
                    break;
     }
    
    
}

bool coupvalide(int colonne) // numero entre 0 et 6 indiquant la colonne jouée
{
    int y;
    int lignefree=-1; // contient le numero de ligne de la case qui est libre pour cette colonne, -1 si la colonne est pleine
 
    colonne -=1; // pour passer de 1..NB_COLONNES à 0..NB_COLONNES-1
 
    if (colonne < 0 || colonne >= NB_COLONNES)
    {
        return FALSE; // si le numero de colonne est en dehors des bornes, le coup n'est pas valide
    }
 
    for ( y = NB_LIGNES-1; y >=0 ; y--)
    {
        if (grille.matrice[y][colonne] == VIDE)
            lignefree = y;
    }
 
    if (lignefree== -1)
    {
        return FALSE; // si la colonne est pleine, le coup n'est pas valide
    }
    return TRUE;
}

// Additioner le contenu de la case adjacente correcte (->meme couleur) au contenu de la case courante.
bool check(){

   
    int ligne, colonne;
    for ( ligne = 0; ligne < NB_LIGNES ; ligne++)    {
        for ( colonne = 0; colonne < NB_COLONNES; colonne++)        {
            /*
            Par defaut il y a 1 pion sur 4 d'aligner correctement

            */
            game[colonne][ligne].horz=1;
            game[colonne][ligne].vert=1;
            game[colonne][ligne].dg=1;
            game[colonne][ligne].dd=1;
        }
    }
 
    // pour le horizontal
    for ( ligne = 0; ligne < NB_LIGNES ; ligne++)    {
        for ( colonne = 1; colonne < NB_COLONNES; colonne++)        {
            if ((grille.matrice[colonne][ligne]==grille.matrice[colonne-1][ligne])&&(grille.matrice[colonne][ligne]!=VIDE))
                // meme couleur
            {
                game[colonne][ligne].horz=game[colonne-1][ligne].horz+1;
                if (game[colonne][ligne].horz==4)
                    return TRUE;
            }
        }
    }
    // pour le vertical
    for ( ligne = 1; ligne < NB_LIGNES ; ligne++)    {
        for ( colonne = 0; colonne < NB_COLONNES; colonne++)        {
            if ((grille.matrice[colonne][ligne]==grille.matrice[colonne][ligne-1])&&(grille.matrice[colonne][ligne]!=VIDE))
                // meme couleur
            {
                game[colonne][ligne].vert=game[colonne][ligne-1].vert+1;
                if (game[colonne][ligne].vert==4)
                    return TRUE;
            }
        }
    }
    // pour le diagonal gauche:de bas à gauche vers haut à droite
    for ( ligne = 1; ligne < NB_LIGNES ; ligne++)    {
        for ( colonne = 0; colonne < NB_COLONNES-1; colonne++)        {
            if ((grille.matrice[colonne][ligne]==grille.matrice[colonne+1][ligne-1])&&(grille.matrice[colonne][ligne]!=VIDE))
                // meme couleur
            {
                game[colonne][ligne].dg=game[colonne+1][ligne-1].dg+1;
                if (game[colonne][ligne].dg==4)
                    return TRUE;
            }
        }
    }
    // pour le diagonal droite:de haut à gauche vers bas à droite
    for ( ligne = 1; ligne < NB_LIGNES ; ligne++)    {
        for ( colonne = 1; colonne < NB_COLONNES; colonne++)        {
            if ((grille.matrice[colonne][ligne]==grille.matrice[colonne-1][ligne-1])&&(grille.matrice[colonne][ligne]!=VIDE))
                // meme couleur
            {
                game[colonne][ligne].dd=game[colonne-1][ligne-1].dd+1;
                if (game[colonne][ligne].dd==4)
                    return TRUE;
            }
        }
    }
    return FALSE;
}


/*------------------------------fonctionnalités de base------------------------------*/

/*1- Placer un jeton dans une colonne de la matrice de jeu
   @do:   modifie la matrice de jeu grille.matrice : variable globale
   @args: colonne numero de la colonne
   @uses: coupvalide(colonne) 
   @return: inteen possible ou pas si la colonne est pleine ou pas */
bool placer_jeton(int colonne ) 
{

  if (coupvalide(colonne)) {
    for (int i=0;i<NB_LIGNES;i++) {
      if (grille.matrice[i][colonne-1]==' ') {
        grille.matrice[i][colonne-1]=Joueurs[leJoueur].jeton;
       
        return TRUE;
      }
    }
  }
  printf("Desole %s, le jeton ne peut etre place...\n", Joueurs[leJoueur].nom);

  return FALSE;
}



/* 2- Tourner un plateau de 90 degrés à droite ou à gauche (les lignes deviennent colonnes)*/
/* @do:     modifie la matrice de jeu grille.matrice : variable globale
   @args:   sens
   @uses:   initMatrice2D() , tmpMat : variable globale
   @return: void */
  void tourner_plateau(int sens)
 {
  char* str=(!sens)?"gauche":"droite";
  printf("%s, tu veux tourner le plateau a %s\n",Joueurs[leJoueur].nom, str);
  initMatrice2D(tmpMat);
  for (int i=0;i<NB_LIGNES;i++) {
    int k=0;
    if (!sens) {
      for (int j=0;j<NB_COLONNES;j++) {
        if (grille.matrice[i][j]==' ') {
          k++; 
        }
        else  {
          grille.matrice[i][j-k] = grille.matrice[i][j];
          tmpMat[j-k][NB_LIGNES-1-i] = grille.matrice[i][j-k];
          grille.matrice[i][j]=' ';
        }
      }
    }
    else if (sens){
      for (int j=NB_COLONNES;j>0;j--) {
        if (grille.matrice[i][j-1]==' ') {
          k++; 
        }
        else {
          grille.matrice[i][j-1+k] = grille.matrice[i][j-1];
          tmpMat[NB_LIGNES-j-k][i] = grille.matrice[i][j-1+k];
          grille.matrice[i][j-1]=' ';
         
        }
      }
    }
  }
  
  initMatrice2D(grille.matrice);
  copyMatrice2D(grille.matrice,tmpMat);

 }


/* 3- Tourner un plateau de 180 degrés (le haut se retrouve en bas)*/
/* @do:     modifie la matrice de jeu grille.matrice : variable globale
   @args:   void 
   @uses:   initMatrice2D() , tmpMat : variable globale
   @return: void */
 void inverser()
 {
    
     printf("%s, tu veux inverser le plateau \n",Joueurs[leJoueur].nom);
    for (int j=0;j<NB_COLONNES;j++) {
      int k=0;
      for (int i=NB_LIGNES-1;i>=0;i--) {
        if (grille.matrice[i][j]==' ') {
          k++; 
        }
        else {
          grille.matrice[i+k][j] = grille.matrice[i][j];
          tmpMat[NB_LIGNES-1-i-k][j] = grille.matrice[i+k][j];
          grille.matrice[i][j]=' ';
        }

      }
    }
    initMatrice2D(grille.matrice);
    copyMatrice2D(grille.matrice,tmpMat);
 }

 /*------------------------------fonctionnalités avancées-----------------------------*/

/* 4- Joueur autonome joue avec intelligence artificielle de niveau 1
   @do:  simule l'urgence: contrer ou gagner le plus vite possible
     1. compare 4 matrices: la normale, celle inversée, celle tournée à droite et celle 
     tournée à gauche.
     2. utilise la fonction check pour savoir ou placer son jeton dans l'urgence
        verifie si le joueur en face est peut aligner 4 pions au coup suivant 
         en utilisant J,I, D ou G
          si oui joue pour le contrer (dans l'ordre J->I->G->D)
          sinon charche à aligner 4 jetons
   @uses: initMatrice2D() , tmpMat : variable globale
   @args: 
   @return: void */
 void JoueurIA(){
   
   char matriceD[NB_LIGNES][NB_COLONNES];
   char matriceG[NB_LIGNES][NB_COLONNES];
   char matriceI[NB_LIGNES][NB_COLONNES];
   tourner_plateau(DROITE);

   tourner_plateau(GAUCHE);

   inverser();

 }


/* 5- Sauvegarde d'une partie en cours
   @do:     copie tous les coups de la partie dans un fichier
   @args:   void 
   @uses:   matrice dynamique qui contient la partie à sauvegarder éventuellement 
   @return: void */
 void sauvegarde(){
  
    FILE* sortie = NULL;
   
    int MAX = (NB_LIGNES*NB_COLONNES+10)*200;
    char buffer[15];
    char nom_sortie[MAX_NOM_FICHIER+1];
    
    do {
        printf("Choisis un nom de fichier (Maximum 15 caractères. Attention, s'il existe, il sera ecrase...:");
        fflush(stdout);
        int j;
        j = scanf(" %s", nom_sortie);
        if (j != 1) {
         printf("Choisis la bonne option stp ;)\n");
         while (!feof(stdin) && !ferror(stdin) && getc(stdin) != '\n');
        }
      } while (strlen(buffer)>15);
   /* time_t timestamp = time( NULL );
    struct tm * pTime = localtime( & timestamp );
    strftime( buffer,15, "%F.txt", pTime );*/

    strcat(nom_sortie,".txt");
    {
      int len;
      do {
        len= strlen(nom_sortie)-1;
        if ((len>=0) && (nom_sortie[len] == '\n'))
          nom_sortie[len]='\0';
      } while ((len<1) && !feof(stdin) && !ferror(stdin));
    }
    if (nom_sortie[0] != '\0') {
      sortie= fopen(nom_sortie, "w");
      fflush(sortie);
      if (sortie==NULL) {
        fprintf(stderr,
                "Erreur : impossible d'ecrire dans le fichier %s\n",
                nom_sortie);
      } else {
            fputs(parties, sortie);
      }
    
    fclose(sortie);
    printf("La partie a ete sauvegardee sous le nom: %s \n",nom_sortie);
    }
  }


 /* 6- Lecture d'une partie sauvegardée
   @do:     lit un fichier et reproduit toutes les grilles avec leurs numéro de coup
      demande à l'utilisateur s'il veut reprendre la partie à la suite ou à un coup choisit

   @args:   void 
   @uses:   fichier externe
   @return: void */
 bool lecture(){
   FILE* entree = NULL;
   char nom_entree[50]; 
   const int MAX = (NB_LIGNES*NB_COLONNES+10)*200;
   char ligne[MAX];
   strcpy(parties,"");
   int nb;
   do {
        printf("Nom du fichier (dans lequel se trouve la partie a charger): ");
        int j;
        j = scanf(" %s", nom_entree);
        if (j != 1) {
         printf("Choisis la bonne option stp ;)\n");
         while (!feof(stdin) && !ferror(stdin) && getc(stdin) != '\n');
        }
      } while (strlen(nom_entree)>50);
    {
    int len;
    strcat(nom_entree,".txt");
      do {
        len= strlen(nom_entree);
        if ((len>=0) && (nom_entree[len-1] == '\n'))
          nom_entree[len-1]='\0';
      } while ((len<1) && !feof(stdin) && !ferror(stdin));
    }
    if (nom_entree[0] != '\0') {
      entree= fopen(nom_entree, "r");
      if (entree==NULL) {
        fprintf(stderr,
                "Erreur : impossible de lire dans le fichier %s\n",
                nom_entree);
        return FALSE;
      } else {

            while (fgets(ligne,MAX-1,entree) != NULL) {
                strcat(parties,ligne);
            }
      } 

    fclose(entree);
    
    }
     return TRUE;
}
 
 