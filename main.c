#include <stdio.h>
#include <stdbool.h>
#include <math.h>

#include "jeu.c" 

#define VIDE ' '
#define ROUGE 'x'
#define JAUNE 'o'

/* Puissance 4 
Ecriture du programme en C
Regle du Jeu:
1-  Placer un jeton de sa couleur dans une colonne à tour de role
2- Celui qui aligne 4 pions de sa couleur a gagné
3- Si toutes les cases sont remplis et qu'aucun joueur n'a réussi, la partie est nulle

Fichiers: jeu.c, jeu.h, main.c

Fonctionnalités:
0- Interface graphique sommaire d'un plateau 7 lignes et 7 colonnes
1- Placer un jeton dans une colonne
2- Tourner un plateau de 90 degrés à droite ou à gauche (les lignes deviennent colonnes)
3- Inverser la vue (retourner le plateau)

Fonctionnalités avancées:
1- Joueur autonome
2- Sauvegarde d'une partie en cours
3- Lecture d'un fichier d'une partie sauvegardée
*/

//structure de données plateau pour enregistrer les valeurs du plateau 




// QUICOMMENCE


/* __________________________________________________________________ */


int nombre1a100(int joueur){
    int nb;
    if (Joueurs[joueur].humain) {
        do {
          printf("%s , tape un nombre entre 1 et 100 s'il te plait: ",Joueurs[joueur].nom);
          fflush(stdout);
          int j;
          j = scanf(" %d",&nb);
          if (j != 1) {
          printf("Choisis la bonne option stp ;)\n"); 
          while (!feof(stdin) && !ferror(stdin) && getc(stdin) != '\n');
          }
        } while ((nb<=0) || (nb>100));
    }
    else {
        printf("A toi de choisir %s !\n", Joueurs[joueur].nom);
        nb=choix_rand(100); 
        sleep(5000);
        printf("Ok! \n");
        sleep(5000);
        printf("Voila! J'ai choisi %d ...\n", nb);
        sleep(2000);
        printf("Merci %s ! \n", Joueurs[joueur].nom);
    }
    return nb;
}

int quicommence(){
    int quichoix;
    int nombre_alea;

    srand (time (NULL));
    
    printf("Je choisis un nombre entre 1 et 100 et celui qui s'en rapproche le plus choisit de commencer ou pas. \n");
    
    //retourne TRUE si joueur1  sinon FALSE
   
    int nb3 = choix_rand(100);
    quichoix = compare(nombre1a100(0),nombre1a100(1),nb3);
    int idJoueurChoisi = !quichoix;
    int idAutreJoueur = quichoix;
    printf("Moi l'ordinateur, j\'avais choisi %d ! \n", nb3);
    printf("C'est toi, %s qui s'est le plus rapproche(e) de ce nombre!\n" ,Joueurs[idJoueurChoisi].nom);
    sleep(3000);
    
    char commence=' ';
    //on  interroge le joueur qui a gagné le droit de choisir 
    printf(" A toi de choisir %s. Tu veux commencer  (O/N) ?  " ,Joueurs[idJoueurChoisi].nom);
    if (Joueurs[idJoueurChoisi].humain == FALSE) {
            commence = 'O';
            sleep(3000);
            printf("O\n");
            printf(" <------- Oui je veux bien! -------> \n");
        }
    else {
         do {
              fflush(stdout);
              int j;
              j = scanf(" %c",&commence);
              if (j != 1) {
              printf("Choisis la bonne option stp ;)\n"); 
              while (!feof(stdin) && !ferror(stdin) && getc(stdin) != '\n');
              }
            } while (commence!='O' && commence!='N');
    }
    

    Joueurs[idJoueurChoisi].jeton = (commence=='O')?'x':'o';
    Joueurs[idAutreJoueur].jeton = (commence=='O')?'o':'x';
    sleep(2000);
    printf("D'accord merci %s! Tu as pour jetons les --> %c <-- !\n",  Joueurs[idJoueurChoisi].nom , Joueurs[idJoueurChoisi].jeton );
    sleep(1000);
    printf("Toi %s, tu as  les --> %c <-- !\n",  Joueurs[idAutreJoueur].nom , Joueurs[idAutreJoueur].jeton );
    
    sleep(1500);
    printf("Le jeu demarre dans quelques instants! \n");
    sleep(4000);
    if (commence=='O') { 
      return idJoueurChoisi;
    } else {
      return idAutreJoueur;
    }
}



// choix des commandes:

void op(){
	char choix; 
    int gameover = FALSE; 
    int leGagnant;
    nbCoups = 0;
	while(choix != 'Q'){

        if (check()==TRUE) {
            gameover = TRUE;
            leGagnant=!leJoueur;
            printf("Victoire de %s au %dieme coup! \n", Joueurs[leGagnant].nom, nbCoups);
            exit (0);
        }else if (nbCoups == MAX_NB_PARTIES){
            gameover = TRUE;
            printf("Match NUL (%d coups) \n", nbCoups);
            exit (0);
        } else {
          printf("Jouons maintenant le coup no %d . \n",++nbCoups);
          demande_coup(&choix);
          do_coup(choix);
          if ((choix!='S')||(choix!='C')) {
          affichage_plateau();
          //on enregistre le coup dans une matrice globale temporaire "parties"
          setMatParties();
          //on passe au joueur suivant
          leJoueur=!leJoueur;
         } else { --nbCoups;} 

        }
	   //Faire un while tant que la touche n'est pas Q ou que la partie est gagné ou perdue (différent de 0)
    }
}

//programme principal
int main(){

     printf("*------------------------------------------------*\n");
     printf("|                PUISSANCE 4                     | \n");
     printf("|               GAZZERA MARCO                    |\n");
     printf("*------------------------------------------------*\n");
    /* __________________________________________________________________ */
    //initialisation de la partie encours:0, gagné:1 ou perdu:2;
    int partie=0;

    initMatrice2D(grille.matrice);
    initMatrice2D(tmpMat);
    //initMatriceParties(parties);

     //initialisation de touche: l'entrée clavier 
    char touche=' ';
    /* __________________________________________________________________ */

    /* __________________________________________________________________ */
    //choix du mode de jeu
    char choix_mode= choix_mode_de_jeu();
    //demande des profils des joueurs
    definition_joueurs(choix_mode); 
    // Debut jeu avec son choix
    leJoueur = quicommence();
    /* __________________________________________________________________ */

    /* __________________________________________________________________ */
    // execution du jeu
    op();

    /* __________________________________________________________________ */
    
    return 0;

}


